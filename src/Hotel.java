import java.util.Scanner;

/**
 * Created by ash on 27.01.17.
 */
public class Hotel {
    public static void main(String[] args) {
        System.out.println("Welcome to our hotel!");
        int[] hotelRooms = new int[100];
        String input = "";
        String inputin = "";
        String inputout = "";
        while (true) {
            System.out.println("Type: rooms if you want to see all rooms in our hotel; ");
            System.out.println("      available if you want to see whether we have any room for you; ");
            System.out.println("      check-in ");
            System.out.println("      check-out ");
            System.out.println("      exit if you want to end this session.");
            System.out.println();
            Scanner in = new Scanner(System.in);
            input = in.next();
            if (input.equals("rooms")) {
                generateHotelandPrint(hotelRooms);
            } else if (input.equals("available")) {
                availableRooms(hotelRooms);
            } else if (input.equals("check-in")) {
                System.out.print("Please write a number of your room using ,: ");
                Scanner in1 = new Scanner(System.in);
                inputin = in1.next();
                checkIn(hotelRooms, inputin);
            } else if (input.equals("check-out")) {
                System.out.print("Please write a number of your rooms using ,: ");
                Scanner in1 = new Scanner(System.in);
                inputout = in1.next();
                checkOut(hotelRooms, inputout);
            } else if (input.equals("exit")) {
                System.out.println("Goodbye");
                return;
            } else System.out.println("Error. Please type one of this words: rooms available check-in check-out exit");
        }
    }

    public static void generateHotelandPrint(int[] hotelRooms) {
        for (int i = 0; i < hotelRooms.length; i++) {
            if (hotelRooms[i] == 0) {
                System.out.print("(Room " + (i + 1) + " occupied) ");
            } else System.out.print("(Room " + (i + 1) + " available) ");
            if ((i + 1) % 10 == 0) {
                System.out.println();
            }
        }
        System.out.println();
    }

    public static void availableRooms(int[] hotelRooms) {
        int print = 0;
        for (int i = 0; i < hotelRooms.length; i++) {
            if (hotelRooms[i] == 1) {
                if (print == 0) {
                    System.out.print("Available rooms: ");
                    print++;
                }
                System.out.print((i + 1) + " ");
            }
        }
        if (print == 0) {
            System.out.println("No rooms available");
        }
        System.out.println();
    }

    public static void checkIn(int[] hotelRooms, String inputin) {
        int print = 0;
        String[] inputStr = inputin.split(",");
        int[] inputarray = new int[inputStr.length];
        for (int a = 0; a < inputStr.length; a++) {
            inputarray[a] = Integer.valueOf(inputStr[a]);
            if (hotelRooms[inputarray[a] - 1] == 1) {
                hotelRooms[inputarray[a] - 1] = 0;
                if (print == 0) {
                    System.out.print("You've checked in rooms: ");
                    print++;
                }
                System.out.print(inputarray[a] + " ");
            }
        }
        if (print == 0) {
            System.out.println("These rooms are not available, please check if we have other rooms");
        }
        else System.out.println();
    }

    public static void checkOut(int[] hotelRooms, String inputout) {
        int print = 0;
        String[] inputStr = inputout.split(",");
        int[] inputarray = new int[inputStr.length];
        for (int a = 0; a < inputStr.length; a++) {
            inputarray[a] = Integer.valueOf(inputStr[a]);
            if (hotelRooms[inputarray[a] - 1] == 0) {
                hotelRooms[inputarray[a] - 1] = 1;
                if (print == 0) {
                    System.out.print("You've checked out of rooms: ");
                    print++;
                }
                System.out.print(inputarray[a]+ " ");
            }
        }
        if (print == 0) {
            System.out.println("Error. These rooms are not occupied");
        }
        else System.out.println();
    }
}
