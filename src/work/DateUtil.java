package work;

import java.util.Calendar;
import java.util.GregorianCalendar;

/* Utilities for Date Manipulation */
public class DateUtil {

    // Month's name – for printing
    public static String strMonths[]
            = {"Jan", "Feb", "Mar", "Apr", "May", "Jun",
            "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};

    // Number of days in each month (for non-leap years)
    public static int daysInMonths[]
            = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    // Returns true if the given year is a leap year
    public static boolean isLeapYear(int year) {
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
            return true;
        }
        return false;
    }

    // Return true if the given year, month, day is a valid date
    // year: 1-9999
    // month: 1(Jan)-12(Dec)
    // day: 1-28|29|30|31. The last day depends on year and month
    public static boolean isValidDate(int year, int month, int day) {
        if (year >= 1 && year <= 9999 && month >= 1 && month <= 12 && day >= 1) {
            if (day == 28 && month == 2 && year % 4 != 0 && year % 100 == 0 && year % 400 != 0) {
                return true;
            }
            if (day == 29 && month == 2 && year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
                return true;
            }
            if (month == 4 || month == 6 || month == 9 || month == 11 && day == 30) {
                return true;
            }
            if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12 && day == 31) {
                return true;
            }

        }
        return false;
    }

    // Return the day of the week, 0:Sun, 1:Mon, ..., 6:Sat
    public static int getDayOfWeek(int year, int month, int day) {
        String[] dayOfWeek = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
        if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
            if (year >= 1700 && year <= 1800) {
                int lastTwoDOfYear = year - 1700;
                if (month == 1) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 2;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 1800 && year <= 1900) {
                int lastTwoDOfYear = year - 1800;
                if (month == 1) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 2;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 1900 && year <= 2000) {
                int lastTwoDOfYear = year - 1900;
                if (month == 1) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 2;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2000 && year <= 2100) {
                int lastTwoDOfYear = year - 2000;
                if (month == 1) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 2;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2100 && year <= 2200) {
                int lastTwoDOfYear = year - 2100;
                if (month == 1) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 2;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2200 && year <= 2300) {
                int lastTwoDOfYear = year - 2200;
                if (month == 1) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 2;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2300 && year <= 2400) {
                int lastTwoDOfYear = year - 2300;
                if (month == 1) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 2;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2400 && year <= 2500) {
                int lastTwoDOfYear = year - 2400;
                if (month == 1) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 2;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
        } else if (year % 4 != 0) {
            if (year >= 1700 && year <= 1800) {
                int lastTwoDOfYear = year - 1700;
                if (month == 1) {
                    month = 0;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 1800 && year <= 1900) {
                int lastTwoDOfYear = year - 1800;
                if (month == 1) {
                    month = 0;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 1900 && year <= 2000) {
                int lastTwoDOfYear = year - 1900;
                if (month == 1) {
                    month = 0;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2000 && year <= 2100) {
                int lastTwoDOfYear = year - 2000;
                if (month == 1) {
                    month = 0;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2100 && year <= 2200) {
                int lastTwoDOfYear = year - 2100;
                if (month == 1) {
                    month = 0;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (4 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2200 && year <= 2300) {
                int lastTwoDOfYear = year - 2200;
                if (month == 1) {
                    month = 0;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (2 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2300 && year <= 2400) {
                int lastTwoDOfYear = year - 2300;
                if (month == 1) {
                    month = 0;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (0 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
            if (year >= 2400 && year <= 2500) {
                int lastTwoDOfYear = year - 2400;
                if (month == 1) {
                    month = 0;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 2) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 3) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 4) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 5) {
                    month = 1;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 6) {
                    month = 4;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 7) {
                    month = 6;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 8) {
                    month = 2;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 9) {
                    month = 5;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 10) {
                    month = 0;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 11) {
                    month = 3;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
                if (month == 12) {
                    month = 5;
                    int sum = (6 + lastTwoDOfYear + lastTwoDOfYear / 4 + month + day) % 7;
                    System.out.println(sum + ": " + dayOfWeek[sum]);
                    return sum;
                }
            }
        }
        return 0;
    }

    // Return String "xxxday d mmm yyyy" (e.g., Wednesday 29 Feb 2012)

    public static String toString(int year, int month, int day) {
        String date = "";
        String[] dayOfWeek = {"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
        date = dayOfWeek[getDayOfWeek(year, month, day)] + " " + day  + " "+  strMonths[month-1] +    " "+ year;
            return date;
    }

    public static String gregorianCalendar (int year, int month, int day) {
        // Construct a Calendar instance with the given year, month and day
        Calendar cal = new GregorianCalendar(year, month - 1, day);  // month is 0-based
// Get the day of the week number: 1 (Sunday) to 7 (Saturday)
        int dayNumber = cal.get(Calendar.DAY_OF_WEEK);
        String[] calendarDays = { "Sunday", "Monday", "Tuesday", "Wednesday",
                "Thursday", "Friday", "Saturday" };
// Print result
        return "It is " + calendarDays[dayNumber - 1];
    }

    public static void main(String[] args) {
        System.out.println(isLeapYear(1900));  // false
        System.out.println(isLeapYear(2000));  // true
        System.out.println(isLeapYear(2011));  // false
        System.out.println(isLeapYear(2012));  // true

        System.out.println(isValidDate(2012, 2, 29));  // true
        System.out.println(isValidDate(2011, 2, 29));  // false
        System.out.println(isValidDate(2099, 12, 31)); // true
        System.out.println(isValidDate(2099, 12, 32)); // false

        System.out.println(getDayOfWeek(1982, 4, 24));  // 6:Sat
        System.out.println(getDayOfWeek(2000, 1, 1));   // 6:Sat
        System.out.println(getDayOfWeek(2054, 6, 19));  // 5:Fri
        System.out.println(getDayOfWeek(2012, 2, 17));  // 5:Fri

        System.out.println(toString(2012, 2, 14)); // Tuesday 14 Feb 2012
        System.out.println(gregorianCalendar(2012, 2, 14));

    }
}