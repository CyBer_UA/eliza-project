package work; /**
 * Created by ash on 21.10.16.
 */

import java.util.Scanner;

public class PalindromicWord {
    public static void main(String[] args) {
        String inStr;        // input String
        String inStrLC;
        int inStrL;
        char in1;
        char in2;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a word: ");
        inStr = in.next();   // use next() to read a String

        inStrLC = inStr.toLowerCase();
        inStrL = inStrLC.length();
        for (int i = inStrL - 1; i >= 0; i--) {
            in2 = inStrLC.charAt(i);
            for (int a = inStrL - (i+ 1); a < inStrL; a++) {
                in1 = inStrLC.charAt(a);
                if (in1 == in2) {
                    break;
                }
                else    System.out.print(inStr + "is not a palindrome");
                }


            }
            System.out.print(inStr + " is a palindrome");
            System.out.println();

        }
    }
