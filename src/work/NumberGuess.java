package work;

import java.util.Scanner;

/**
 * Created by ash on 09.12.16.
 */
public class NumberGuess {
    public static void main(String[] args) {
        int secretNumber = (int) (Math.random() * 100);
            Scanner in = new Scanner(System.in);
            System.out.println("Key in your guess: ");
        int trials = 0;
        while (secretNumber != 0) {
            trials += 1;
            int input = in.nextInt();
            if(input < secretNumber){
                System.out.println("Try higher");
            }
            else if(input == secretNumber){
                System.out.println("You got it in "+ trials +" trials!");
            }
            else  System.out.println("Try lower");
        }
    }
}
