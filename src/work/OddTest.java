package work;

import java.util.Scanner;

/**
 * Created by ash on 04.11.16.
 */
public class OddTest {
    public static void main(String[] args) {
        int inp;
        while (true) {
            System.out.println("Print a number: ");
            Scanner in = new Scanner(System.in);
            inp = in.nextInt();
            isOdd(inp);

        }
    }

    public static boolean isOdd(int inp) {
        if (inp % 2 == 0) {
            System.out.println("EVEN");
            return false;
        } else {
            System.out.println("ODD");
            return true;
        }

    }
}


//    Exercise (Method): Write a boolean method called isOdd() in a class called work.OddTest, which takes an int as input and returns true if the it is odd. The signature of the method is as follows:
//public static boolean isOdd(int number)
//        Also write the main() method that prompts user for a number, and prints "ODD" or "EVEN". You should test for negative input.
