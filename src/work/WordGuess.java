package work;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 * Created by ash on 10.12.16.
 */
public class WordGuess {
    public static void main(String[] args) {
//        String secretWord = "testing";
        String secretWord = "";
        try {
            List<String> wordList = getWordList();
            int randomNum = (int)(Math.random() * wordList.size());
            secretWord = wordList.get(randomNum).toLowerCase();
        } catch (FileNotFoundException err) {
            System.out.println(err);
        }


        String[] secretWordArray = secretWord.split("");
        Scanner in = new Scanner(System.in);
        System.out.println("Key in one character or your guess word: ");
        int trials = 0;

        String[] guessLetters = new String[secretWordArray.length];
               for (int a = 0; a < secretWordArray.length; a++) {

            guessLetters[a] = "_";
        }
        while (secretWordArray.length != 0) {
            trials += 1;
            String input = in.nextLine().toLowerCase();
            String[] inputArray = input.split("");
            String guessWordstr = "";
            if (inputArray.length == 1) {
                for (int i = 0; i < secretWordArray.length; i++) {
                    if (input.charAt(0) == secretWord.charAt(i)) {
                        guessLetters[i] = secretWordArray[i];
                    }
                }
                for (int r = 0; r < guessLetters.length; r++){
                    guessWordstr += guessLetters[r];
                }
                System.out.println("Trial " + trials + " :" + guessWordstr);
                System.out.println("Key in one character or your guess word: ");
            }
            else if (inputArray.length == secretWordArray.length){
                int sum = 0;
                for(int b = 0; b < secretWordArray.length; b++){

                   if(input.charAt(b) == secretWord.charAt(b)){
                       sum +=1;
                       continue;
                   }
                    else System.out.println("You're not right. Try again");
                    break;
                }
                if (sum == secretWordArray.length){System.out.println("Congratulation!");
                System.out.println("You got it in " + trials + " trials!");
                break;}
            }else System.out.println("Try again");
        }
    }

    private static List<String> getWordList() throws FileNotFoundException {
        Scanner s = new Scanner(new File("wordlist"));
        ArrayList<String> list = new ArrayList<String>();
        while (s.hasNext()){
            list.add(s.next());
        }
        s.close();
        return  list;
    }
}
