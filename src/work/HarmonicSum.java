package work;

public class HarmonicSum {   // Save as "work.HarmonicSum.java"
    public static void main(String[] args) {
        double sum = 0.0;
        double sum1 = 0.0;
        double sum2 = 0.0;
        int maxDenominator = 10000000;
        for (int denominator = 3; denominator <= maxDenominator; denominator += 2) {  // 1, 3, 5, 7,...
            if (denominator % 4 == 1) {
                sum1 += 1 / (double)denominator;
            } else if (denominator % 4 == 3) {
                sum2 -= 1 / (double)denominator;
            } else {   // remainder of 0 or 2
                System.out.println("The computer has gone crazy?!");
            }

        }
        System.out.println(4 * (1 +  sum2 + sum1));
    }
}
