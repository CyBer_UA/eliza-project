package work;

import java.util.Scanner;

/**
 * Created by lh on 07.12.16.
 */
public class PrintTriangles {
    public static void main(String[] args) {
        PrintTriangles print = new PrintTriangles();
        System.out.println("Enter numRows: ");
        Scanner in = new Scanner(System.in);
        int numRows = in.nextInt();
        print.PowerOf2Triangle(numRows);
    }

    public static void PowerOf2Triangle(int numRows) {
        for (int i = 1; i <= numRows; i++) {
            for (int k = i; k < numRows; k++) {
                System.out.print("    ");
            }
            int g = 1;
            for (int a = 1; a <= i; a++) {
                if (a == 1) {
                    System.out.print(g + "   ");
                } else {
                    g = g * 2;
                    if (g >= 10) {
                        System.out.print(g + "  ");
                    } else if (g >= 100) {
                        System.out.print(g + "");
                    } else if (g >= 1000) {
                        System.out.print(g + "");
                    } else System.out.print(g + "   ");

                }
            }

            for (int k = i - 1; k > 0; k--) {
                if (k == 1) {
                    System.out.print(k + "    ");
                } else {
                    g = g / 2;
                    if (g >= 10) {
                        System.out.print(g + "   ");
                    } else if (g >= 100) {
                        System.out.print(g + " ");
                    } else if (g >= 1000) {
                        System.out.print(g+ " ");
                    } else System.out.print(g + " 8  ");
                }
            }

            System.out.println();
        }
    }
}
