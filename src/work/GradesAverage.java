package work;

import java.util.Scanner;

/**
 * Created by ash on 02.11.16.
 */
public class GradesAverage {
    public static void main(String[] args) {
        int numStudents;
        int gradeSt;
       double average =0;
        int sum = 0;
        int a;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of students: ");
        numStudents = in.nextInt();
        int[] grades = new int[numStudents];
        int j = 0;
        for(int i =1; i<= numStudents; i++){
            System.out.print("Enter the grade for student "+i+ " : ");
            gradeSt = in.nextInt();
            if (gradeSt > 0 && gradeSt<=100){
                grades[j] = gradeSt;
                a = grades[j];
                j++;
                sum = sum +a;
            }
            else {
                System.out.print("Invalid grade, try again...");
                System.out.println();
            i--;}

        }
        average = sum/numStudents;
        System.out.println("The average is: "+ average);


    }
}
