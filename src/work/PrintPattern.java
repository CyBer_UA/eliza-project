package work; /**
 * Created by lh on 07.12.16.
 */

import java.util.Scanner;

/**
 * Created by lh on 02.12.16.
 */
public class PrintPattern {
    public static void main(String[] args) {
        PrintPattern print = new PrintPattern();
        System.out.println("Enter a positive integer: ");
        Scanner in = new Scanner(System.in);
        int size = in.nextInt();
        print.printPatternL(size);
    }

    public static void printPatternA(int size) {
        for (int i = size; i >= 0; i--) {

            for (int k = i; k >= size; k--) {
                System.out.print("  ");
            }
            for (int a = size; a > 0; a -= 1) {
                System.out.print("# ");
            }
            size -= 2;
            System.out.println();
        }
    }

    public static void printPatternB(int size) {
        for (int k = size / 2; k >= 0; k--) {
            for (int i = k; i >= 0; i--) {
                System.out.print("  ");
            }
            for (int a = k * 2; a < size; a += 1) {
                System.out.print("# ");
            }
            System.out.println();
        }

    }

    public static void printPatternC(int size) {
        for (int k = (size / 2); k >= 0; k--) {
            for (int i = k; i >= 0; i--) {
                System.out.print("  ");
            }
            for (int a = k * 2; a < size; a += 1) {
                System.out.print("# ");
            }
            System.out.println();
        }
        for (int i = size; i >= 0; i--) {

            for (int k = i + 1; k >= size; k--) {
                System.out.print("  ");
            }
            for (int a = size - 2; a > 0; a -= 1) {
                System.out.print("# ");
            }
            size -= 2;
            System.out.println();
        }

    }

    public static void printPatternD(int size) {
        for (int i = 1; i <= size; i++) {
            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
    }

    public static void printPatternE(int size) {
        for (int i = size; i >= 0; i--) {
            for (int k = i; k < size; k++) {
                System.out.print("  ");
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }

            System.out.println();
        }
    }

    public static void printPatternG(int size) {
        for (int i = size; i > 0; i--) {
            for (int k = i; k > 0; k--) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
    }

    public static void printPatternF(int size) {
        for (int i = 1; i <= size; i++) {
            for (int k = i; k < size; k++) {
                System.out.print("  ");
            }

            for (int k = i; k > 0; k--) {
                System.out.print(k + " ");
            }

            System.out.println();
        }
    }

    public static void printPatternH(int size) {
        for (int i = 1; i <= size; i++) {
            for (int k = i; k < size; k++) {
                System.out.print("  ");
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }
            for (int k = i - 1; k > 0; k--) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
    }

    public static void printPatternI(int size) {
        for (int i = size; i >= 0; i--) {
            for (int k = i; k < size; k++) {
                System.out.print("  ");
            }
            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }
            for (int k = i - 1; k > 0; k--) {
                System.out.print(k + " ");
            }
            System.out.println();
        }
    }

    public static void printPatternJ(int size) {
        for (int i = 1; i <= size; i++) {
            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }
            for (int k = i * 2 + 1; k < size * 2; k++) {
                System.out.print("  ");
            }
            for (int k = i; 0 < k; k--) {
                if (k < size) {
                    System.out.print(k + " ");
                }
            }
            System.out.println();
        }
    }

    public static void printPatternK(int size) {
        for (int i = size; i >= 0; i--) {

            for (int k = 1; k <= i; k++) {
                System.out.print(k + " ");
            }
            for (int k = i * 2 + 1; k < size * 2; k++) {
                System.out.print("  ");
            }
            for (int k = i; k > 0; k--) {
                if (k < size) {
                    System.out.print(k + " ");
                }
            }
            System.out.println();
        }
    }

    public static void printPatternL(int size) {
        for (int i = 1; i <= size; i++) {

            for (int k = i; k < size; k++) {
                System.out.print("  ");
            }
            for (int k = i; k < i * 2; k++) {
                if (k >= 10) {
                    int g = k - 10;
                    System.out.print(g + " ");
                } else System.out.print(k + " ");
            }

            for (int k = (i * 2)-2; k >= i; k--) {
                if (k >= 10) {
                    int g = k - 10;
                    System.out.print(g + " ");
                } else System.out.print(k + " ");
            }
            System.out.println();
        }

    }
}



