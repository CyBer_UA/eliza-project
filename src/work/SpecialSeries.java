package work;

/**
 * Created by ash on 09.12.16.
 */
public class SpecialSeries {
    public static void main(String[] args) {
        SpecialSeries aSpecialSeries = new SpecialSeries();

        double x = 0.1D;
        System.out.printf("Sum of the series: %.9f \n"
                , aSpecialSeries.sumOfSeries(x, 5));
    }

    public static double sumOfSeries(double x, int numTerms) {
        double sum = x;
        double multiply = 1;

        for (double i = 0; i < numTerms * 2; i = i + 2) {
            multiply *= ((i + 1.0) / (i + 2.0));
            double pow = i + 3;
            sum += (multiply * (Math.pow(x, pow) / pow));
        }
        return sum;
    }
}
