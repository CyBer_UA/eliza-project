package work;

import java.util.Scanner;

/**
 * Created by ash on 04.11.16.
 */
public class MagicSum {
    public static void main(String[] args) {
        int sum = 0;
        String inp;
        int sentinel = -1;
        Scanner in = new Scanner(System.in);
        System.out.println("Enter a positive integer or -1 to exit: ");
        inp = in.next();
        while (Integer.valueOf(inp) != sentinel) {
            if (hasEight(inp)==true) {
                sum += Integer.valueOf(inp);
            }
            System.out.println("Enter a positive integer or -1 to exit: ");
            inp = in.next();
        }
        System.out.println(" The magic sum is: " + sum);
    }

    public static boolean hasEight(String inp) {

        int length = inp.length();
        String[] inpa = inp.split("");
        for (int i = 0; i < length; i++) {
            if (Integer.valueOf(inpa[i]) == 8) {
                return true;
            }

        }
        return false;

    }
}


