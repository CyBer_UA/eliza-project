package work;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by lh on 17.01.17.
 */
public class MapFrance {

    public static void main(String args[]) {
        MapFrance obj = new MapFrance();
        String[] cities = {"Warsaw", "Lublin", "Brest", "Rennes", "Paris", "Caen", "Nantes", "Calais", "Nancy", "Strasbourg", "Dijon", "Lyon", "Grenoble", "Avignon", "Limoges", "Marseille", "Montpellier", "Nice", "Cracow", "Toulouse", "Gdansk", "Hel", "Bordeaux"};
        Scanner in = new Scanner(System.in);
        System.out.println(Arrays.toString(cities));
        System.out.println("Start city: ");
        String startcityinput = in.next();
        Scanner in1 = new Scanner(System.in);
        System.out.println("Finish city: ");
        String finishcityinput = in1.next();
        int source = 0;
        int target = 0;
        int sum = 0;
        int sum1 = 0;
        for (int i = 0; i < cities.length; i++) {
            if (startcityinput.length() == cities[i].length()) {
                for (int b = 0; b < startcityinput.length(); b++) {
                    if (startcityinput.charAt(b) == cities[i].charAt(b)) {
                        sum += 1;
                        continue;
                    } else break;
                }
                if (sum == startcityinput.length()) {
                    source = i;
                    System.out.println(source);
                    break;
                }
            } else continue;
        }
        for (int i = 0; i < cities.length; i++) {
            if (finishcityinput.length() == cities[i].length()) {
                for (int b = 0; b < finishcityinput.length(); b++) {
                    if (finishcityinput.charAt(b) == cities[i].charAt(b)) {
                        sum1 += 1;
                        continue;
                    } else break;
                }
                if (sum1 == finishcityinput.length()) {
                    target = i;
                    System.out.println(target);
                    break;
                }
            } else continue;
        }
//        if (startcityinput == null || finishcityinput == null || source == 0 || target == 0) {
//            System.out.println("City is not on the list");
//        }
        int[] neighbourssource = obj.neighbours(source, cities);
        int[] searchpath = obj.search(source, target, neighbourssource, cities);
        String searchpathstr = "";
        for (int a = 0; a < searchpath.length; a++) {
            int num = searchpath[a];
            searchpathstr += cities[num] + " ";
        }
        System.out.println(searchpathstr);
    }


    public static int[] neighbours(int source, String[] cities) {
        ////warsaw
        int[] neighbourssource = {};
        if (source == 0) {
            int[] neighbours = {1, 169};
            neighbourssource = neighbours;
            return neighbourssource;
        }
////brest
        else if (source == 2) {
            int[] neighbours = {3, 243};
            neighbourssource = neighbours;
            return neighbourssource;
        }
////rennes
        else if (source == 3) {
            int[] neighbours = {4, 349, 5, 185, 2, 242, 6, 113};
            neighbourssource = neighbours;
            return neighbourssource;
        }
        return neighbourssource;
    }

    public static int[] search(int source, int target, int[] neighbourssource, String[] cities) {
        int[] searchpath = {};

        for (int i = 0; i < neighbourssource.length; i = i + 2) {
            if (source == target) {
                System.out.println("The cities are the same");
                return searchpath;
            } else if (target == neighbourssource[i]) {
                int[] searchpath1 = {source, target};
                searchpath = searchpath1;
            }
        }
        if (searchpath.length == 0) {
            int sourcevalue = 0;
            int targetvalue = 0;
            int[] citiesvalue = {};
            searchpath[0] = source;
            int[] citiesnum = {};
            for (int d = 0; d < cities.length; d++) {
                citiesvalue[d] = 0;
            }
            for (int e = 0; e < neighbourssource.length; e++) {
                int numneib = neighbourssource[e];
                citiesvalue[numneib] += 1;
            }
            if (citiesvalue[target] == 1) { searchpath[searchpath.length- 1] = target;
            } else source = neighbourssource[0];
        }

        return searchpath;
    }

    ////caen
//        addLane("Edge_6", 5, 7, 347);
//        addLane("Edge_7", 5, 4, 234);
//        addLane("Edge_8", 5, 3, 185);
////callais
//        addLane("Edge_9", 7, 8, 477);
//        addLane("Edge_10", 7, 4, 293);
//        addLane("Edge_11", 7, 5, 347);
////nancy
//        addLane("Edge_12", 8, 9, 150);
//        addLane("Edge_13", 8, 10, 213);
//        addLane("Edge_14", 8, 4, 336);
//        addLane("Edge_15", 8, 7, 477);
////strasbourg
//        addLane("Edge_16", 9, 10, 330);
//        addLane("Edge_17", 9, 8, 150);
////dijon
//        addLane("Edge_18", 10, 9, 330);
//        addLane("Edge_19", 10, 11, 195);
//        addLane("Edge_20", 10, 4, 315);
//        addLane("Edge_21", 10, 8, 213);
////lyon
//        addLane("Edge_22", 11, 12, 111);
//        addLane("Edge_23", 11, 13, 229);
//        addLane("Edge_24", 11, 14, 410);
//        addLane("Edge_25", 11, 10, 195);
////grenoble
//        addLane("Edge_26", 12, 13, 221);
//        addLane("Edge_27", 12, 11, 111);
////avignon
//        addLane("Edge_28", 13, 12, 221);
//        addLane("Edge_29", 13, 15, 103);
//        addLane("Edge_30", 13, 16, 98);
//        addLane("Edge_31", 13, 11, 229);
////marseille
//        addLane("Edge_32", 15, 17, 207);
//        addLane("Edge_33", 15, 13, 103);
////nice
//        addLane("Edge_34", 17, 15, 207);
//        addLane("Edge_35", 17, 18, 1597);
//        //montpellier
//        addLane("Edge_36", 16, 13, 98);
//        addLane("Edge_37", 16, 19, 242);
////toulouse
//        addLane("Edge_38", 19, 16, 242);
//        addLane("Edge_39", 19, 22, 246);
//        addLane("Edge_40", 19, 14, 291);
////bordeaux
//        addLane("Edge_41", 22, 14, 227);
//        addLane("Edge_42", 22, 19, 246);
//        addLane("Edge_43", 22, 6, 353);
////limoges
//        addLane("Edge_44", 14, 11, 410);
//        addLane("Edge_45", 14, 19, 291);
//        addLane("Edge_46", 14, 22, 227);
//        addLane("Edge_47", 14, 18, 1857);
//        addLane("Edge_48", 14, 6, 319);
//        addLane("Edge_49", 14, 4, 394);
////nantes
//        addLane("Edge_50", 6, 14, 319);
//        addLane("Edge_51", 6, 22, 353);
//        addLane("Edge_52", 6, 3, 113);
////paris
//        addLane("Edge_53", 4, 7, 293);
//        addLane("Edge_54", 4, 8, 336);
//        addLane("Edge_55", 4, 10, 315);
////        addLane("Edge_56", 4, 14, 394);
//    addLane("Edge_57",4,3,349);
//
//    //        addLane("Edge_58", 4, 5, 234);
//////crakow
////        addLane("Edge_59", 18, 20, 583);
//////gdansk
//    addLane("Edge_60",20,21,101);
//
//    for(
//    int i = 0;
//    i<cities.length;i++)
//
//    {
//
//    }

}


//    }
