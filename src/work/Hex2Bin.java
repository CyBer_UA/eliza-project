package work;

import java.util.Scanner;

/**
 * Created by ash on 03.11.16.
 */
public class Hex2Bin {
    public static void main(String[] args) {
        String hexStr;
        int length;
        int c;
        String a;
        String line = " ";
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a Hexadecimal string: ");
        hexStr = in.next();
        length = hexStr.length();
        hexStr.toLowerCase();
        String[] hexBits1 = {"0000", "0001", "0010", "0011",
                "0100", "0101", "0110", "0111",
                "1000", "1001", "1010", "1011",
                "1100", "1101", "1110", "1111"};
        for (int pos = 0; pos < length; ++pos) {
            int order = length - 1 - pos;
            char hexChar = hexStr.charAt(pos);
            int[] hexBits = new int[length];
            // 23 cases: '0'-'9', 'a'-'f', 'A'-'F', others (error)
            if (hexChar >= '0' && hexChar <= '9') {
                c = (hexChar - '0');
                a = hexBits1[c];
                line = line + a + " ";
            } else if (hexChar >= 'a' && hexChar <= 'f') {   // lowercase
                c = (hexChar - 'a' + 10);
                a = hexBits1[c];
                line = line + a + " ";
            } else {
                System.out.println("error: invalid hexadecimal string");
                System.exit(1);    // quit the program
            }

        }
        System.out.print(" The equivalent binary for hexadecimal " + hexStr + " is: " + line);
    }
}
