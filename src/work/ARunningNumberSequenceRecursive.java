package work;

/**
 * Created by ash on 16.12.16.
 */
public class ARunningNumberSequenceRecursive {
    public static void main(String args[]) {
        int n = 17;
        System.out.println(numberSequenceRecurs(n, 1, ""));
        System.out.println(numberSequenceIter(n));
    }

    public static int numberSequenceRecurs(int n, int i, String numbers) {
        if (i > n) {
            return numbers.length();
        }

        return numberSequenceRecurs(n, i + 1, numbers + i);
    }

    public static int numberSequenceIter(int n) {
        String numbers = "";
        for (int i = 1; i <= n; i++) {
            numbers += i;
        }
        System.out.println("S(" + n + ") = " + numbers);
        return numbers.length();
    }
}
