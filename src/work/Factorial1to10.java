package work;

/**
 * Created by ash on 09.12.16.
 */
public class Factorial1to10 {
    public static void main(String args[]) {
        int numFactorials = 13;
        int fact = 1;
        for (int i = 1; i <= numFactorials; i++) {
            fact *= i;
            if (fact > 2147483646 || fact < -2147483647) {
                System.out.println("The factorial of " + i + " is out of range");
            } else {
                System.out.println("The factorial of " + i + " is " + fact);
            }
        }
    }
}
