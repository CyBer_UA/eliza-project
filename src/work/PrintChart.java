package work;

import java.util.Scanner;

/**
 * Created by lizah on 21.11.16.
 */
public class PrintChart {
    public static void main(String[] args) {
        int bars;
        int value;
        Scanner in = new Scanner(System.in);
        System.out.print(" Enter number of bars: ");
        bars = in.nextInt();
        int[] barsArray = new int[bars];
        int j = 0;
        for (int i = 1; i <= bars; i++) {
            System.out.print(" Enter bar " + i + " value: ");
            value = in.nextInt();
            if (value > 0 && value <= 100) {
                barsArray[j] = value;
                j++;
            } else {
                System.out.print("Invalid grade, try again...");
                System.out.println();
                i--;
            }

        }

        for (int a = 0; a < barsArray.length; a++) {
            for (int k = 0; k < barsArray[a]; k++) {
                System.out.print("*");
            }
            System.out.print("(" + barsArray[a] + ")  \n,/" );
        }
    }
}