package work;

/**
 * Created by ash on 16.12.16.
 */
public class GCDRecursive {
    public static void main(String args[]) {
        int a = 20;
        int b = 25;
        System.out.println(gcd(a,b));
    }

    public static int gcd(int a, int b) {
        if (b == 0) {
            return a;
        }
        return  gcd(b, remainder(a,b));
    }
    public static int remainder(int a, int b) {
        return a%b;
    }
}
