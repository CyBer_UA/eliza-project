package work;

/**
 * Created by ash on 24.11.16.
 */
public class PrintAnimalPattern {
    public static void main(String[] args) {
        String s = "\\";
        System.out.println("          '__'");
        System.out.println("          (©©)");
        System.out.println("  "+"/"+"========"+s+"/");
        System.out.println(" / || %% ||");
        System.out.println("*  ||----||");
        System.out.println("   ¥¥    ¥¥");
        System.out.println("   \"\"    \"\"");
    }
}
