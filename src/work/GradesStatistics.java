package work;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ash on 12.11.16.
 */
public class GradesStatistics {
    public static int[] grades;  // Declare an int[], to be allocated later.
    // This array is accessible by all the methods.

    public static void main(String[] args) {
        readGrades();  // Read and save the inputs in int[] grades
        printArray(grades);
        System.out.println("The average is " + average(grades));
        System.out.println("The median is " + median(grades));
        System.out.println("The minimum is " + min(grades));
        System.out.println("The maximum is " + max(grades));
        System.out.println("The standard deviation is " + stdDev(grades));
    }

    // Prompt user for the number of students and allocate the global "grades" array.
    // Then, prompt user for grade, check for valid grade, and store in "grades".
    public static void readGrades() {
        int numStudents;
        int gradeSt;
        int sum = 0;
        int a;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter the number of students: ");
        numStudents = in.nextInt();
        grades = new int[numStudents];
        int j = 0;
        for (int i = 1; i <= numStudents; i++) {
            System.out.print("Enter the grade for student " + i + " : ");
            gradeSt = in.nextInt();
            if (gradeSt > 0 && gradeSt <= 100) {
                grades[j] = gradeSt;
                a = grades[j];
                j++;
                sum = sum + a;
            } else {
                System.out.print("Invalid grade, try again...");
                System.out.println();
                i--;
            }
        }
    }

    // Print the given int array in the form of {x1, x2, x3,..., xn}.

    public static void printArray(int[] array) {
        PrintArray.printArray(array);
    }

    // Return the average value of the given int[]
    public static double average(int[] array) {
        double average = 0;
        int numStudents;
        int sum = 0;
        int a;
        for (int i = 0; i < array.length; i++) {
            a = array[i];
            sum = sum + a;
        }
        average = (double) sum / array.length;
        return average;
    }


    // Return the median value of the given int[]
    // Median is the center element for odd-number array,
    // or average of the two center elements for even-number array.
    // Use Arrays.sort(anArray) to sort anArray in place.
    public static double median(int[] array) {
        double median = 0;
        Arrays.sort(array);
        if (array.length % 2 == 0) {
            int indexcentreElem1 = array.length / 2 - 1;
            int indexcentreElem2 = array.length / 2;
            median = (double) (array[indexcentreElem1] + array[indexcentreElem2]) / 2;
        } else {
            int indexcentreElem = (array.length / 2) + 1;
            median = (double) (array[indexcentreElem]);
        }
        return median;
    }


    // Return the maximum value of the given int[]
    public static int max(int[] array) {
        int max = array[0];   // Assume that max is the first element
        // From second element, if the element is more than max, set the max to this element.
        for (int i = 1; i < array.length; i++) {
            if (max < array[i]) {
                max = array[i];
            }
        }
        return max;
    }

    // Return the minimum value of the given int[]
    public static int min(int[] array) {
        int min = array[0];   // Assume that min is the first element
        // From second element, if the element is less than min, set the min to this element.
        for (int i = 1; i < array.length; i++) {
            if (min > array[i]) {
                min = array[i];
            }
        }
        return min;
    }

    //    // Return the standard deviation of the given int[]
    public static double stdDev(int[] array) {
        double aver = average(array);
        double sum = 0.0;
        for (int i = 0; i < array.length; i++) {
            sum += Math.pow(array[i] - aver, 2);
        }
        double stdDev = Math.sqrt(sum / array.length);
        DecimalFormat decim = new DecimalFormat("0.00");
        Double  stdDev2 = Double.parseDouble(decim.format(stdDev));
        return stdDev2;
    }
}