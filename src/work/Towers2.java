package work; /**
 * Created by lh on 31.10.16.
 */

import java.util.Scanner;

public class Towers2 {


    public static void main(String[] args) {
        String n;
        String a = "";
        int b;
        Scanner in = new Scanner(System.in);
        Towers2 t = new Towers2();
        System.out.println("Please write arguments split with , :");
        n = in.next();
        String[] towersArgs = n.split(",");

        if (towersArgs.length == 1) {
            a = t.towers2(Integer.valueOf(towersArgs[0]));
        } else if (towersArgs.length == 2) {
            a = t.towers2(Integer.valueOf(towersArgs[0]), Integer.valueOf(towersArgs[1]));

        } else if (towersArgs.length == 3) {
            a = t.towers2(Integer.valueOf(towersArgs[0]), Integer.valueOf(towersArgs[1]), Integer.valueOf(towersArgs[2]));

        } else if (towersArgs.length == 4) {
            a = t.towers2(Integer.valueOf(towersArgs[0]), Integer.valueOf(towersArgs[1]), Integer.valueOf(towersArgs[2]), Integer.valueOf(towersArgs[3]));

        }

        System.out.println(a);
    }

    private String towers2(int n, int start, int stop, int finish) {
        if (n == 1) {
            System.out.println("I am moving the top disk of the tower " + start + " to the tower " + finish);
            return null;
        } else {
            towers2(n - 1, start, finish, stop);
            towers2(1, start, stop, finish);
            towers2(n - 1, stop, start, finish);
            return "Done!";

        }
    }

    public String towers2(int n) {
        return towers2(n, 1, 2, 3);
    }

    public String towers2(int n, int start) {
        return towers2(n, start, 2, 3);
    }

    public String towers2(int n, int start, int stop) {
        return towers2(n, start, stop, 3);
    }

}
