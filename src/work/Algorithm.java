package work;

/**
 * Created by ash on 24.12.16.
 */
public class Algorithm {
    public static void main(String args[]) {
        int[] array = {20, 11, 18, 14, 15, 9, 32, 5, 26};
        int key = 1;
        int fromIdx = 0;
        int toIdx = array.length - 1;

//        System.out.println(linearSearch(array, key));
//        System.out.println(linearSearch1(array, key));
//        System.out.println(binarySearch(array, key, fromIdx, toIdx));
//        System.out.println(bubbleSort(array));
//        System.out.println(selectionSort(array));
//        System.out.println(insertionSort(array));
   //     System.out.println(mergeSort(array));
    }

    // Return the array index, if key is found; or 0 otherwise
    public static int linearSearch(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                return key;
            }
        }
        return -1;
    }

    // Return true if the key is found
    public static boolean linearSearch1(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                return true;
            }
        }
        return false;
    }

    // Return true if key is found in the array in the range of fromIdx (inclusive), toIdx (exclusive)
    public static boolean binarySearch(int[] array, int key, int fromIdx, int toIdx) {
        int middleIdx = (fromIdx + toIdx) / 2;
        if (array[fromIdx] == array[toIdx]) {  /* one-element list */
            if (key == array[fromIdx]) {
                return true;
            } else return false;
        } else if (key == array[middleIdx]) {
            return true;
        } else if (key < array[middleIdx]) {
            toIdx = middleIdx;
        } else fromIdx = middleIdx + 1;
        return binarySearch(array, key, fromIdx, toIdx);
    }

    // Return true if key is found in the array
    public static boolean binarySearch(int[] array, int key) {
        return binarySearch(array, key, array[0], array[array.length - 1]);
    }

    public static String bubbleSort(int[] array) {
        int n = array.length;
        String arraystr = "";
        int swap;/* boolean flag to indicate swapping occurred during a pass */
        while ( /* a pass */ n != 0) { /* reset for each pass */
            for (int i = 1; i < n; i++) {
       /* if this pair is out of order */
                if (array[i - 1] > array[i]) {
         /* swap them and take note something changed */
                    swap = array[i - 1];
                    array[i - 1] = array[i];
                    array[i] = swap;

                }

            }
            n = n - 1; /* One item sorted after each pass */
        }
        while (n == 0) {
            break;
        }  /* repeat another pass if swapping occurred */
        for (int r = 0; r < array.length; r++) {
            arraystr += array[r] + " ";
        }
        return arraystr;
    }

    public static String selectionSort(int[] array) {
        String arraystr = "";
        for (int min = 0; min < array.length - 1; min++) {
            int least = min;
            for (int j = min + 1; j < array.length; j++) {
                if (array[j] < array[least]) {
                    least = j;
                }
            }
            int tmp = array[min];
            array[min] = array[least];
            array[least] = tmp;
        }
        for (int r = 0; r < array.length; r++) {
            arraystr += array[r] + " ";
        }
        return arraystr;
    }

    public static String insertionSort(int[] array) {
        int key = 0;
        String arraystr = "";
        int i = 0;
        for (int j = 2; j < array.length; j++) {
            key = array[j];
            i = j - 1;
            while (i > 0 && array[i] > key) {
                array[i + 1] = array[i];
                i = i - 1;
            }
            array[i + 1] = key;
        }
        for (int r = 0; r < array.length; r++) {
            arraystr += array[r] + " ";
        }
        return arraystr;
    }

    public static String quickSort(int[] array) {
        String arraystr = "";
        recursiveQuickSort(array, 0, array.length - 1);
        for (int r = 0; r < array.length; r++) {
            arraystr += array[r] + " ";
        }
        return arraystr;
    }

    public static void recursiveQuickSort(int[] array, int startIdx, int endIdx) {
        int idx = partition(array, startIdx, endIdx);
        // Recursively call quicksort with left part of the partitioned array
        if (startIdx < idx - 1) {
            recursiveQuickSort(array, startIdx, idx - 1);
        } // Recursively call quick sort with right part of the partitioned array
        if (endIdx > idx) {
            recursiveQuickSort(array, idx, endIdx);
        }
    }

    public static int partition(int[] array, int left, int right) {
        int pivot = array[left]; // taking first element as pivot
        while (left <= right) { //searching number which is greater than pivot, bottom up
            while (array[left] < pivot) {
                left++;
            } //searching number which is less than pivot, top down
            while (array[right] > pivot) {
                right--;
            } // swap the values
            if (left <= right) {
                int tmp = array[left];
                array[left] = array[right];
                array[right] = tmp; //increment left index and decrement right index
                left++;
                right--;
            }
        }
        return left;
    }
//
//    public static String mergeSort(int[] array) {
//        String arraystr = "";
//        recursiveMergeSort(array, 0, array.length - 1);
//        for (int r = 0; r < array.length; r++) {
//            arraystr += array[r] + " ";
//        }
//        return arraystr;
//    }


}




