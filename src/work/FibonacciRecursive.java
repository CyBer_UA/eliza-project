package work;

/**
 * Created by ash on 16.12.16.
 */
public class FibonacciRecursive {
    public static void main(String args[]) {
        int n = 11;
        System.out.println(fibonacci(n));
    }

    public static int fibonacci(int n) {
        if (n == 0) {
            return 0;
        }
        if (n == 1) {
            return 1;
        } else
            return fibonacci(n - 1) + fibonacci(n - 2);
    }

}
