package work;

import java.util.Scanner;

public class CheckVowelsDigits {
    public static void main(String[] args) {
        String inStr;        // input String
        int inStrLen;
        int sumDigits = 0;
        int sumvowels = 0;
        int sumCons = 0;
        char c;
        double prDigits;
        double prVowels;

        Scanner in = new Scanner(System.in);
        System.out.print("Enter a String: ");
        inStr = in.next();   // use next() to read a String
        inStrLen = inStr.length();


        for (int i = inStrLen - 1; i >= 0; --i) {  // Process the String from the right
            c = inStr.charAt(i);
            if (c >= '0' && c <= '9') {
                sumDigits += 1;
            } else if (c == 'a') {
                sumvowels += 1;
            } else sumCons += 1;


        }
        prDigits = (double) sumDigits * 100 / inStrLen;
        prVowels = (double) sumvowels * 100 / inStrLen;
        System.out.print("Number of vowels: " + sumvowels + " (" + String.format("%.02f", prVowels) + "%)");

        System.out.println();
        System.out.print("Number of digits " + sumDigits + " (" + String.format("%.02f", prDigits) + "%)");
    }
}



