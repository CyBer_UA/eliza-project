package work; /**
 * Created by lh on 31.10.16.
 */

import java.util.Scanner;

public class KK {
    public static void main(String[] args) {
        String puzzle;
        System.out.println("Please use the LISP notation: and, or, implies, iff.");
        System.out.println("As a variable please use A (Alice), B (Bob), C (Celine).");
        System.out.println("Write the code of the puzzle here: ");
        Scanner in = new Scanner(System.in);
        puzzle = in.next();
        String[] puzzleArgs = puzzle.split(" ");
        for (int i = 0; i < puzzleArgs.length; i++) {
            String[] item = puzzleArgs[i].split("-");
            if (item[0] == "A" && item[1] == "no") {
                System.out.println("Alice is a knave");
            }

            if (item[0] == "B" && item[1] == "no") { System.out.println("Bob is a knave");}

            if (item[0] == "C" && item[1] == "no") { System.out.println("Celine is a knave");}
        }
    }
}
