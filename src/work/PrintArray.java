package work;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by ash on 05.11.16.
 */
public class PrintArray {
    public static void main(String[] args) {
        int[] array1 = {1, 2, 3, 4, 5};
        int[] array2 = {6, 7, 8, 9, 10};
        System.out.println(swap(array1,array2));
    }

    public static void printArray(int[] array) {
        System.out.print("{");
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }
        System.out.print("}");
        System.out.println();
    }

    public static void printArray(double[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }

    }

    public static void printArray(float[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + ",");
        }

    }

    public static String arrayToString(int[] array) {
        String s = java.util.Arrays.toString(array).replaceAll(",", ",");
        return s;
    }

    public static boolean contains(int[] array, int key) {
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                return true;
            }
        }
        return false;
    }

    public static int search(int[] array, int key) {
        int k = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == key) {
                k = i;
                return k;
            }
        }
        return -1;

    }

    public static boolean equals(int[] array1, int[] array2) {
        if (array1.length == array2.length) {
            for (int i = 0; i < array1.length; i++) {
                if (array1[i] == array2[i]) {
                } else return false;
            }
            return true;
        }
        return false;
    }

    public static int[] copyOf(int[] array) {
        int[] copy = array.clone();
        return copy;
    }

    public static int[] copyOf(int[] array, int newLength) {
        int[] copy = Arrays.copyOfRange(array, 0, newLength);

        return copy;
    }
    public static void reverse(int[] array){
        int[] arrayReverse = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            int i1 = array.length - i -1;
            arrayReverse[i] = array[i1];
        }
        printArray(arrayReverse);
    }
    public static boolean swap(int[] array1, int[] array2){
        if(array1.length== array2.length){
            int number;
            for (int i = 0; i < array1.length; i++) {
                number = array1[i];
                array1[i] = array2[i];
                array2[i] = number;
            }
           printArray(array1);
            System.out.println();
            printArray(array2);
            return true;
        }
        return false;
    }

}




