package work; /**
 * Created by ash on 21.10.16.
 */

import java.util.Scanner;

public class PhoneKeyPad {
    public static void main(String[] args) {
        String inStr;        // input String
        String inStrLC;
        int inStrL;
        char in1;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a String: ");
        inStr = in.next();   // use next() to read a String
        inStrLC = inStr.toLowerCase();
        inStrL = inStrLC.length();
        for (int i = 0; i < inStrL; i++) {
            in1 = inStrLC.charAt(i);
            switch (in1) {  // switch on a char
                case 'a':
                case 'b':
                case 'c':
                    System.out.print(2);
                    break;
                case 'd':
                case 'e':
                case 'f':

                    System.out.print(3);
                    break;
                case 'g':
                case 'h':
                case 'i':

                    System.out.print(4);
                    break;
                case 'j':
                case 'k':
                case 'l':

                    System.out.print(5);
                    break;
                case 'm':
                case 'n':
                case 'o':

                    System.out.print(6);
                    break;
                case 'p':
                case 'q':
                case 'r':
                case 's':

                    System.out.print(7);
                    break;
                case 't':
                case 'u':
                case 'v':

                    System.out.print(8);
                    break;
                case 'w':
                case 'x':
                case 'y':
                case 'z':

                    System.out.print(9);
                    break;
                default:

                    System.out.print("It wasn't a letter");
            }


            System.out.println();
        }
    }

}

////' On your phone keypad, the alphabets are mapped to digits as follows: ABC(2), DEF(3), GHI(4), JKL(5), MNO(6), PQRS(7), TUV(8), WXYZ(9).
//                    //      Write a program called work.PhoneKeyPad, which prompts user for a String (case insensitive), and converts to a sequence of keypad digits.
//                    //    Use a nested-if (or switch-case) in this exercise.
//                    Hints:
////  You can use in.next().toLowerCase() to read a String and convert it to lowercase to reduce your cases.
////In switch, you can handle multiple cases, e.g.,
////switch (inChar) {  // switch on a char
////case 'a': case 'b': case 'c':
////System.out.print(2); break;
////case 'd': case 'e': case 'f':
////......
////default:
//
