package work; /**
 * Created by lh on 31.10.16.
 */

import java.util.Scanner;

public class Towers {


    public static void main(String[] args) {
        String n;
        String result = "";
        Scanner in = new Scanner(System.in);
        Towers object = new Towers();
        System.out.println("Please write arguments split with , :");
        n = in.next();    // read input
        String[] towersArgs = n.split(",");

        //depend on quantity of parameters, we call method on object, with parameters from user input
        if (towersArgs.length == 1) {
            result = object.towers( Integer.valueOf(towersArgs[0]) );// integer from string
        } else if (towersArgs.length == 2) {
            result = object.towers(Integer.valueOf(towersArgs[0]), Integer.valueOf(towersArgs[1]));

        } else if (towersArgs.length == 3) {
            result = object.towers(Integer.valueOf(towersArgs[0]), Integer.valueOf(towersArgs[1]), Integer.valueOf(towersArgs[2]));

        } else if (towersArgs.length == 4) {
            result = object.towers(Integer.valueOf(towersArgs[0]), Integer.valueOf(towersArgs[1]), Integer.valueOf(towersArgs[2]), Integer.valueOf(towersArgs[3]));

        }
        

        System.out.println(result);
    }

    private String towers(int n, int start, int stop, int finish) {
        if (n == 0) {
            return "Done!";
        } else {
            towers(n - 1, start, finish, stop);
            System.out.println("I am moving the top disk of the tower " + start + " to the tower " + finish);
            return towers(n - 1, stop, start, finish);

        }
    }

    public String towers(int n) {
        return towers(n, 1, 2, 3);
    }

    public String towers(int n, int start) {
        return towers(n, start, 2, 3);
    }

    public String towers(int n, int start, int stop) {
        return towers(n, start, stop, 3);
    }

}
