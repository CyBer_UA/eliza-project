package work;

import java.util.Scanner;

/**
 * Created by ash on 02.11.16.
 */
public class Hex2Dec {
    public static void main(String[] args) {
        String hexStr;
        char hexChar;
        int hexStrLen;
        int c;
        int sum = 0;
        Scanner in = new Scanner(System.in);
        System.out.print("Enter a Hexadecimal String: ");
        hexStr = in.next();
        hexStrLen = hexStr.length();
        if (hexStrLen > 2) {
            System.out.println("error: invalid hexadecimal string");
            return;
        }
        for (int pos = 0; pos < hexStrLen; ++pos) {
            hexChar = hexStr.charAt(pos);
// 23 cases: '0'-'9', 'a'-'f', 'A'-'F', others (error)
            if (hexChar >= '0' && hexChar <= '9') {
                c = (hexChar - '0');
                sum = sum + c + 16 - 1;
            } else if (hexChar >= 'a' && hexChar <= 'f') {   // lowercase
                c = (hexChar - 'a' + 10);
                sum = sum + c;
                System.out.println("The equivalent decimal number for hexadecimal " + hexStr + " is: " + sum);
            } else if (hexChar >= 'A' && hexChar <= 'F') {   // uppercase (use in.next().toLowerCase() to eliminate this)
                c = (hexChar - 'A' + 10);
                sum = sum + c;
                System.out.println("The equivalent decimal number for hexadecimal " + hexStr + " is: " + sum);
            } else {
                System.out.println("error: invalid hexadecimal string");
                System.exit(1);    // quit the program
            }
        }
    }
}
