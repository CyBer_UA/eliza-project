//package graph;
//
//import java.util.ArrayList;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Scanner;
//
//public class TestDijkstraAlgorithm {
//
//    private static List<Vertex> cities;
//    private static List<Edge> edges;
//
//
//
//        cities = new ArrayList<Vertex>();
//        edges = new ArrayList<Edge>();
//        Vertex source = null;
//        Vertex target = null;
//
//
//        cities.add(new Vertex("0", "Warsaw"));
//        cities.add(new Vertex("1", "Lublin"));
//        cities.add(new Vertex("2", "Brest"));
//        cities.add(new Vertex("3", "Rennes"));
//        cities.add(new Vertex("4", "Paris"));
//        cities.add(new Vertex("5", "Caen"));
//        cities.add(new Vertex("6", "Nantes"));
//        cities.add(new Vertex("7", "Calais"));
//        cities.add(new Vertex("8", "Nancy"));
//        cities.add(new Vertex("9", "Strasbourg"));
//        cities.add(new Vertex("10", "Dijon"));
//        cities.add(new Vertex("11", "Lyon"));
//        cities.add(new Vertex("12", "Grenoble"));
//        cities.add(new Vertex("13", "Avignon"));
//        cities.add(new Vertex("14", "Limoges"));
//        cities.add(new Vertex("15", "Marseille"));
//        cities.add(new Vertex("16", "Montpellier"));
//        cities.add(new Vertex("17", "Nice"));
//        cities.add(new Vertex("18", "Cracow"));
//        cities.add(new Vertex("19", "Toulouse"));
//        cities.add(new Vertex("20", "Gdansk"));
//        cities.add(new Vertex("21", "Hel"));
//        cities.add(new Vertex("22", "Bordeaux"));
//
//
//        Scanner in = new Scanner(System.in);
//        System.out.println("Start city: ");
//        String input1 = in.next();
//        Scanner in1 = new Scanner(System.in);
//        System.out.println("Finish city: ");
//        String input2 = in1.next();
//        for (int i = 0; i < cities.size(); i++) {
//            if (cities.get(i).getName().equals(input1)) {
//                source = cities.get(i);
//            }
//
//            if (cities.get(i).getName().equals(input2)) {
//                target = cities.get(i);
//            }
//
//        }
//        if (source == null || target == null) {
//            System.out.println("Failure");
//            return;
//        }
////warsaw
//        addLane("Edge_0", 0, 1, 169);
////brest
//        addLane("Edge_1", 2, 3, 243);
////rennes
//        addLane("Edge_2", 3, 4, 349);
//        addLane("Edge_3", 3, 5, 185);
//        addLane("Edge_4", 3, 2, 242);
//        addLane("Edge_5", 3, 6, 113);
////caen
//        addLane("Edge_6", 5, 7, 347);
//        addLane("Edge_7", 5, 4, 234);
//        addLane("Edge_8", 5, 3, 185);
////callais
//        addLane("Edge_9", 7, 8, 477);
//        addLane("Edge_10", 7, 4, 293);
//        addLane("Edge_11", 7, 5, 347);
////nancy
//        addLane("Edge_12", 8, 9, 150);
//        addLane("Edge_13", 8, 10, 213);
//        addLane("Edge_14", 8, 4, 336);
//        addLane("Edge_15", 8, 7, 477);
////strasbourg
//        addLane("Edge_16", 9, 10, 330);
//        addLane("Edge_17", 9, 8, 150);
////dijon
//        addLane("Edge_18", 10, 9, 330);
//        addLane("Edge_19", 10, 11, 195);
//        addLane("Edge_20", 10, 4, 315);
//        addLane("Edge_21", 10, 8, 213);
////lyon
//        addLane("Edge_22", 11, 12, 111);
//        addLane("Edge_23", 11, 13, 229);
//        addLane("Edge_24", 11, 14, 410);
//        addLane("Edge_25", 11, 10, 195);
////grenoble
//        addLane("Edge_26", 12, 13, 221);
//        addLane("Edge_27", 12, 11, 111);
////avignon
//        addLane("Edge_28", 13, 12, 221);
//        addLane("Edge_29", 13, 15, 103);
//        addLane("Edge_30", 13, 16, 98);
//        addLane("Edge_31", 13, 11, 229);
////marseille
//        addLane("Edge_32", 15, 17, 207);
//        addLane("Edge_33", 15, 13, 103);
////nice
//        addLane("Edge_34", 17, 15, 207);
//        addLane("Edge_35", 17, 18, 1597);
//        //montpellier
//        addLane("Edge_36", 16, 13, 98);
//        addLane("Edge_37", 16, 19, 242);
////toulouse
//        addLane("Edge_38", 19, 16, 242);
//        addLane("Edge_39", 19, 22, 246);
//        addLane("Edge_40", 19, 14, 291);
////bordeaux
//        addLane("Edge_41", 22, 14, 227);
//        addLane("Edge_42", 22, 19, 246);
//        addLane("Edge_43", 22, 6, 353);
////limoges
//        addLane("Edge_44", 14, 11, 410);
//        addLane("Edge_45", 14, 19, 291);
//        addLane("Edge_46", 14, 22, 227);
//        addLane("Edge_47", 14, 18, 1857);
//        addLane("Edge_48", 14, 6, 319);
//        addLane("Edge_49", 14, 4, 394);
////nantes
//        addLane("Edge_50", 6, 14, 319);
//        addLane("Edge_51", 6, 22, 353);
//        addLane("Edge_52", 6, 3, 113);
////paris
//        addLane("Edge_53", 4, 7, 293);
//        addLane("Edge_54", 4, 8, 336);
//        addLane("Edge_55", 4, 10, 315);
//        addLane("Edge_56", 4, 14, 394);
//        addLane("Edge_57", 4, 3, 349);
//        addLane("Edge_58", 4, 5, 234);
////crakow
//        addLane("Edge_59", 18, 20, 583);
////gdansk
//        addLane("Edge_60", 20, 21, 101);
//
//        // Lets check from location Loc_1 to Loc_10
//        Graph graph = new Graph(cities, edges);
//        DijkstraAlgorithm dijkstra = new DijkstraAlgorithm(graph);
//
//        dijkstra.execute(source);
//        LinkedList<Vertex> path = dijkstra.getPath(target);
//        if (path == null) {
//            System.out.println("Failure");
//            return;
//        }
//
//        int sum = 0;
//
//        for (int i = 0; i < path.size() - 1; i++) {
//            for (int j = 0; j < edges.size(); j++) {
//                if (edges.get(j).getSource() == path.get(i) &&
//                        edges.get(j).getDestination() == path.get(i + 1)) {
//                    System.out.println(path.get(i).getName());
//                    System.out.println(edges.get(j).getWeight() + " km");
//                    sum += edges.get(j).getWeight();
//
//                }
//            }
//        }
//
//        System.out.println(path.getLast().getName());
//        System.out.println("from " + path.getFirst().getName() + " to " + path.getLast().getName() + " " + sum + " km");
//
//
//    }
//
//    private static void addLane(String laneId, int sourceLocNo, int destLocNo,
//                                int duration) {
//        Edge lane = new Edge(laneId, cities.get(sourceLocNo), cities.get(destLocNo), duration);
//        edges.add(lane);
//    }
//}